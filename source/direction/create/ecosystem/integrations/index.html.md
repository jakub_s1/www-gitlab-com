---
layout: markdown_page
title: "Category Direction - Integrations"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Create](/direction/dev/#create-1) | 
| **Maturity** | [Viable](/handbook/product/categories/maturity/) |
| **Last reviewed** | 2021-01-16 |

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics/1515)
- [Overall Ecosystem Direction](/direction/create/ecosystem/)

_This direction is a work in progress, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and 
  epics on this category page. Sharing your feedback directly on GitLab.com is 
  the best way to contribute to our vision.
* Alternately, please share feedback directly via [email](mailto:pdeuley@gitlab.com) or 
  [Twitter](https://twitter.com/gitlab). If you're a GitLab user and have direct
  knowledge of your need from a particular integration, we'd love to hear from you.

## Overview

GitLab's vision is to be the best [single application for every part of the DevOps toolchain](/handbook/product/single-application/). 
However, some customers use tools other than our included features, [and we respect those decisions](/handbook/product/gitlab-the-product/#plays-well-with-others).
Currently, GitLab offers [30+ Integrations](https://docs.gitlab.com/ee/user/project/integrations/overview.html)
that work with a variety of external systems. Integrations are important to 
GitLab's success, and the **Integrations category** was established to develop and
maintain these integrations with key 3rd party systems and services.

This category will primarily focus on creating new integrations that support the
needs of enterprise customers, as those customers often have hard integration-related 
requirements that can fully prevent them from adopting GitLab. By supporting 
these requirements, we unlock new parts of the market which are otherwise 
wholly inaccessible.

## Direction

The **Integrations** direction is to support GitLab's efforts at making our 
application _[Enterprise Ready](https://about.gitlab.com/direction/#1-year-plan)_ 
by focusing on integrations that are of the highest value to our largest customers.

Many enterprise organizations rely on systems like Jira, Jenkins, and 
ServiceNow, and it is often a _hard requirement_ to have a robust integration with 
these services. _Not_ having that integration can block adoption of GitLab completely.

By making these integrations powerful and useful, we make the lives of our 
users better&mdash;even when they're using other products. This is what we mean 
when we say that GitLab [plays well with others](/handbook/product/gitlab-the-product/#plays-well-with-others).

Particularly for large organizations, existing tools and services 
can be extremely difficult to migrate off of, even without any explicit vendor 
lock-in. Moving _thousands of users_ or _hundreds of thousands_ of existing files 
or objects off of one system to GitLab can incur more costs than might be 
obvious at first. Internal tools may be tightly knit with the other internal 
systems meaning numerous new integrations have to be developed just to keep 
the business running.

While GitLab has a robust API that supports integrating _almost anything you may need_, 
it's a much more powerful experience to have a _native_ integration available out of the box.

## Maturity

The Integrations category tracks [Maturity](/direction/maturity/) 
on a per-integration basis. Each integration is evaluated based on the following 
criteria:

* A **Minimal** integration meets a single basic need for a small set of 
  customers, and may only push data one way from one system to the other without 
  surfacing much data or functionality directly in the UI.
* A **Viable** integration meets the core needs of most customers, and is robust 
  or configurable enough to meet all the needs of some customers.
* A **Complete** integration meets the needs of the vast majority of usecases 
  for the majority of users, and the integration allows users to work painlessly 
  between the two products.
* A **Lovable** integration not only meets the needs of the vast majority of 
  users, but it makes the experience of using both products as productive and 
  easy as possible. This may mean things like special consideration taken to 
  intra-product navigation and how we surface notifications from the other 
  service, for example.

## Current High-priority Integrations

_You can view a list of all of our current integrations on our [Integrations page](https://docs.gitlab.com/ee/user/project/integrations/overview.html)_

| Integration           | Maturity Level      | Documentation                                                                               | Issues / Planning Epic    |
| ---                   | ---                 | ---                                                                                         | --- |
| Webhooks              | Viable              | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)         | [Open Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Integration%3A%3Awebhooks) |
| Atlassian Jira        | Viable              | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/jira.html)             | [Open Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Integration%3A%3AJira) |
| Slack                 | Viable              | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/slack.html)            | [Open Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Integration%3A%3ASlack) |
| Jenkins               | Viable              | [Documentation](https://docs.gitlab.com/ee/integration/jenkins.html)                        | [Open Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Integration%3A%3AJenkins) |
| ServiceNow            | Minimal             | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/servicenow.html)       | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1712) |
| Microsoft Teams       | Minimal             | [Documentation](https://docs.gitlab.com/ee/user/project/integrations/microsoft_teams.html)  | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/880) |
| Rally                 | Under Consideration | _n/a_                                                                                       | [Issue](https://gitlab.com/gitlab-org/gitlab/issues/169) |
| Jama                  | Under Consideration | _n/a_                                                                                       | [Issue](https://gitlab.com/gitlab-org/gitlab/issues/16182) |

## Prioritization

We prioritize our Integrations work based on:

* Reach (Customer demand) -- We want work on what the majority of our customers 
  need. We evaluate this based number of current users impacted, how much an 
  addition would increase our Total Addressable Market, and external market trends.
* Impact -- Integrations that are vital to an organizations processes and 
  workflow are also the ones most likely to be hard requirements for adoption. 
  It's unlikely that a simple notification integration would block adoption, 
  but not supporting Change Management tooling very much can.
* Effort (Integration complexity) -- The amount of work and time it would take 
  to complete that work absolutely factors in to priority. We have a limited 
  amount of time and resources, so we have to choose our work wisely.

Based on the above scope and these priorities, we're currently only targeting a 
limited set of products and services--specifically, those listed above and those 
that are [scheduled on our backlog](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations&milestone_title=Any). 
All prioritized integrations have a target maturity of `Complete`, and will 
take precedent over other integrations until they hit that maturity level.

_For all other integrations, our team also supports our [API](/handbook/direction/create/ecosystem/api) 
so that developers can build anything else they need._

## What's next and why

Over the next year, Ecosystem will be focused on the following work:

### Make Jira and GitLab work well in concert

Jira is one of our most popular integrations, and a common thread we hear is that "developers want
to be able to stay in GitLab", and not need to visit Jira to do daily tasks. The goal of our upcoming work is to get the features to a point where a typical developer can stay in GitLab for the majority of their Jira needs.

* [Add Issue Detail view from Jira Issue List](https://gitlab.com/gitlab-org/gitlab/-/issues/220999) (13.8 - 13.11)
* [Combine installation process of GitLab integration and the Jira for GitLab.com app](https://gitlab.com/gitlab-org/gitlab/-/issues/225803) (13.9 - 13.10)

### Improve our Slack integration

Slack notifications are the most common integration on GitLab projects, giving 
users the ability to send important activity to the relevant channels in their Workspace.
We also have a lightweight Slack application that supports a variety of ChatOps related Slash Commands

* [Displaying helpful text when linking to private repos](https://gitlab.com/gitlab-org/gitlab/-/issues/14194)
* [Fine-grained Slack Notifications](https://gitlab.com/gitlab-org/gitlab/-/issues/18278)
* [Unfurl links to code in Slack](https://gitlab.com/gitlab-org/gitlab/-/issues/215143)

### Improve our Webhooks

Webhooks are a generic way for projects to be integrated with any other service. GitLab's APIs allow other services to _reach in to_ our data, Webhooks proactively send data to another service when certain events happen. These are increasingly important for external vendors, as they offer a key way to integrate with GitLab that doesn't require them building inside our codebase.

* [Open Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Integration%3A%3Awebhooks)

## What we're not doing

### Building a "marketplace"

GitLab does not utilize a plugin model for integrations with other common tools 
and services, or provide a marketplace for them. As an [open core project](https://en.wikipedia.org/wiki/Open-core_model), 
integrations can live directly inside the product. Learn more about our reasons 
for this in our [Product Handbook](/handbook/product/product-principles/#avoid-plugins-and-marketplaces).

This does not mean we will **never** build a "marketplace" for GitLab, it 
just means we have no intention of doing it at this time.

### Integrating "everything"

There are dozens of products and services that customers have requested that 
we build an integration with, and we sincerely wish we had the time and funding 
to be able to build all of them. However, since we are a team of limited size 
and there are only so many hours in a day, we focus on on creating 
the integrations requested by our biggest customers and userbases.

However, we're happy to [partner with your company](/partners/integrate/) if 
you'd like to contribute an integration with your product. As an [open core project](https://en.wikipedia.org/wiki/Open-core_model),
anyone in our community is welcome to add the integrations they need.

## Contributing

This category develops and maintains specific integrations inside the GitLab 
codebase, but that doesn't preclude you and your team from adding your own. At 
GitLab, one of our values is that everyone can contribute. If you're looking 
to contribute your own integration, or otherwise get involved with features in 
the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AIntegrations).

Feel free to reach out to the team directly if you need guidance or want 
feedback on your work by pinging [@deuley](https://gitlab.com/deuley) or 
[@gitlab-ecosystem-team](https://gitlab.com/gitlab-org/ecosystem-team) on your 
open MR.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Partnership

If your company is interested in partnering with GitLab, check out the [Partner with GitLab](https://about.gitlab.com/partners/integrate/) 
page for more info.

## Integration design guidelines

Special considerations apply to integrations that don't apply to building native functionality. The
product handbook [has a set of recommendations and guidelines](/handbook/product/product-principles/#integrate-other-applications-thoughtfully) 
to consider when working on these types of projects.

## Feedback & Requests

If there's an integration that you'd like to see GitLab offer, or if you have 
feedback about an existing integration: please [submit an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?issue%5Btitle%5D=New%20GitLab%20Integration%20with) 
with the label `~Category:Integrations`, or ping [@deuley](https://gitlab.com/deuley) on any relevant issues.
